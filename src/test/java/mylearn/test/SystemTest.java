
package mylearn.test;

import java.io.File;
import java.util.StringTokenizer;

import org.apache.catalina.startup.CatalinaProperties;
import org.junit.Before;
import org.junit.Test;

public class SystemTest {

    @Before
    public void init() {
        System.setProperty("catalina.base", System.getProperty("user.dir"));
        System.setProperty("catalina.home", System.getProperty("catalina.base"));
    }
    
    @Test
    public void testUserDir() throws Exception {
        //当前运行的目录
        String property = System.getProperty("user.dir");
        System.out.println(property);
        
        //获取上级目录
        File file = new File(property, "..");
        System.out.println(file.getCanonicalPath());
    }
    
    @Test
    public void testStringTokenizer() throws Exception {
        String value = CatalinaProperties.getProperty("common" + ".loader");
        System.out.println(value);
        StringTokenizer tokenizer = new StringTokenizer(value, ",");
        while(tokenizer.hasMoreElements()){
//            String nextElement = (String) tokenizer.nextElement();
//            System.out.println(nextElement);
            
            String nextToken = tokenizer.nextToken();
            System.out.println(nextToken);
        }
        
    }
    
    
    
    public static void main(String[] args) {
        //System.out.println(args.length);
        //从VM参数获取
        System.out.println(System.getProperty("catalina.config"));
        //从env参数获取
        System.out.println(System.getenv("catalina.config"));
    }
    
}

